# Create opensearch clusters


## Getting started

ansible-galaxy install -r requirements.yml

В group_vars/all/clusters.yml добавить в список clusters описание нового кластера:  
name имя кластера  
user учетная запись администратора. используется для задачи предварительной проверки кластера  
password пароль учетной записи администратора (в формате ansible-vault encrypt_string)  
kibana_user учетная запись фронтэнда. для плдключения Opensearch Dashboards  
kibana_password пароль учетной записи фронтэнда (в формате ansible-vault encrypt_string)  
internal_users блок описания внутренних учетных записей в файле internal_users.yml. используется при инициализации кластера. для каждой учетной записи необходимо указать пароль в виде hash. hash генерируется на любом существующем инстансе с помощью штатной утилиты ```OPENSEARCH_JAVA_HOME=/usr/share/opensearch/jdk bash /usr/share/opensearch/plugins/opensearch-security/tools/hash.sh```  
node_key ключ транспортного сертификата  
node_pem транспортный сертификат  
auth.ldap.bind_dn технологическая учетная запись для подключения кластера к домену (в формате LDAP)  
auth.ldap.password пароль технологической учетной записи (в формате ansible-vault encrypt_string)  
